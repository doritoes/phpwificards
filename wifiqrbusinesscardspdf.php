<?php
/* make a printable PDF document of business cards with WiFI information
   including a scannable QR code */

require('phpqrcode/qrlib.php');
require('fpdf.php');
define('FPDF_FONTPATH','.');
$pdf = new FPDF('P', 'in', 'Letter');

// globals
$leftmargin = 0.75;
$rightmargin = 0.75; // unused
$topmargin = 0.50;
$bottommargin = 0.50; // unused
$margin = 0.25; // the margin on the card itself
$cardwidth = 3.5;
$cardheight = 2.0;

function DrawBoxes($startX, $startY) { // for debugging
    global $pdf, $margin, $leftmargin, $topmargin, $cardwidth, $cardheight;
    $pdf->Rect($startX, $startY, $cardwidth, $cardheight);
    $pdf->Rect($startX + $margin, $startY + $margin, $cardwidth - (2 * $margin), $cardheight - (2 * $margin));
}

function DrawFront($startX, $startY, $title, $datestring, $ssid, $password) {
    global $pdf, $margin, $leftmargin, $topmargin, $cardwidth, $cardheight;
    // draw title
    $pdf->SetXY($startX + $margin, $startY + $margin);
    $pdf->SetFont('CalibriBold','B',20);
    $pdf->Cell($cardwidth - (2 * $margin), 20/72, $title,0,2,'C');
    $pdf->SetFont('Calibri','',11);
    // draw updated flourish
    $pdf->SetXY($startX + $margin, $pdf->GetY() + 0.09375 ); // was 0.125
    // Thickness of frame (0.1 mm)
    $pdf->SetLineWidth(0.005);
    $pdf->Cell($cardwidth - (2 * $margin), 16/72,"Updated $datestring",'TB',2,'C');
    // tricky bit - laying out the ssid and password in different sizes
    $nextY = $pdf->GetY();
    $ssidfontsize = 20;
    $passwordfontsize = 20;
    $pdf->SetFont('Calibri','',14);
    $lefttextwidth = $pdf->GetStringWidth('Password:');
    // shrink the font size for ssid if needed
    $ssiddone = 0;
    while ($ssiddone == 0 && $ssidfontsize > 9) {
        $pdf->SetFont('Calibri','',$ssidfontsize);
        if ($pdf->GetStringWidth($ssid) > ($cardwidth - ((2 * $margin) + $lefttextwidth + 0.125 + 0.0625))) {
            $ssidfontsize--;
        } else {
            $ssiddone = 1;
        }
    }
    // shrink the font size for password if needed
    $passworddone = 0;
    while ($passworddone == 0 && $passwordfontsize > 9) {
        $pdf->SetFont('Calibri','',$passwordfontsize);
        if ($pdf->GetStringWidth($password) > ($cardwidth - ((2 * $margin) + $lefttextwidth + 0.125 + 0.0625))) {
            $passwordfontsize--;
        } else {
            $passworddone = 1;
        }
    }
    $spacing = ((($cardheight - ($nextY - $startY)) - $margin) - ($ssidfontsize/72+$passwordfontsize/72)) / 3;
    $firstlineY = $nextY + $spacing;
    $pdf->SetFont('Calibri','',$ssidfontsize);
    $ssdwidth = $pdf->GetStringWidth($ssid);
    $pdf->SetFont('Calibri','',$passwordfontsize);
    $passwordwidth = $pdf->GetStringWidth($password);
    $righttextwidth = max($ssdwidth, $passwordwidth);
    $totalwidth = $lefttextwidth + 0.125 + $righttextwidth;
    $lefttextX = $startX + (($cardwidth - $totalwidth) / 2);
    $righttextX = $lefttextX + $lefttextwidth + 0.125;
    // draw SSID
    $pdf->SetFont('Calibri', '', 14);
    $pdf->SetXY($lefttextX, $firstlineY + (($ssidfontsize-14)/144));
    $pdf->Cell($lefttextwidth, 14/72, 'SSID:');
    $pdf->SetFont('Calibri', '', $ssidfontsize);
    $pdf->SetXY($righttextX, $firstlineY);
    $pdf->Cell($pdf->GetStringWidth($ssid), $ssidfontsize/72, $ssid);
    // draw password
    $pdf->SetFont('Calibri', '', 14);
    $pdf->SetXY($lefttextX, $firstlineY + $spacing + ($ssidfontsize/72) + (($passwordfontsize-14)/144));
    $pdf->Cell($lefttextwidth, 14/72, 'Password:');
    $pdf->SetFont('Calibri', '', $passwordfontsize);
    $pdf->SetXY($righttextX, $firstlineY + $spacing + ($ssidfontsize/72));
    $pdf->Cell($pdf->GetStringWidth($password), $passwordfontsize/72, $password);
}

function DrawBack($startX, $startY, $ssid, $pass, $disclaimer, $type = 'qr') { // send type = 'simple' for simple card back
    global $pdf, $margin, $leftmargin, $topmargin, $cardwidth, $cardheight;
    // draw disclaimer
    $pdf->SetXY($startX + $margin, $startY + $margin);
    if ($type == 'qr') {
        $disclaimerfontsize = 11;
        $pdf->SetFont('Calibri','',$disclaimerfontsize);
        $pdf->MultiCell(($cardwidth - (2 * $margin)) - 1.25, ($disclaimerfontsize + 1)/72,$disclaimer, 0);
        $qifi = "WIFI:T:WPA;S:$ssid;P:$pass;;";
        DrawQR(($startX + $cardwidth - $margin) - 1, $startY + $margin, 1, $qifi);
    } else {
        $disclaimerfontsize = 12;
        $pdf->SetFont('Calibri','',$disclaimerfontsize);
        $pdf->MultiCell($cardwidth - (2 * $margin), ($disclaimerfontsize + 1)/72,$disclaimer, 0);
    }
}

function DrawQR($startX, $startY, $width, $string) {
    global $pdf;
     // generating frame 
    $frame = QRcode::text($string, false, QR_ECLEVEL_M); 
    // drawing QR code in pdf with rectangles 
    $h = count($frame); 
    $w = strlen($frame[0]);
    $pixelwidth = $width / $w;
    for($y=0; $y<$h; $y++) { 
        for($x=0; $x<$w; $x++) { 
            if ($frame[$y][$x] == '1') {
                $pdf->Rect($startX + ($x * $pixelwidth), $startY + ($y * $pixelwidth), $pixelwidth, $pixelwidth,'F');
            } 
        } 
    } 
}

function DrawSSID($title, $date, $ssid, $password, $disclaimer, $type = 'qr') {
    global $pdf, $leftmargin, $topmargin, $cardwidth, $cardheight;
    // Page 1
    $pdf->AddPage();
    $pdf->AddFont('Calibri','','calibri.php');
    $pdf->AddFont('CalibriBold','B','calibri.php');
    for ($rows = 0; $rows < 5; $rows++) {
        $cardY = $topmargin + ($rows * $cardheight);
        for ($cols = 0; $cols < 2; $cols++) {
            $cardX = $leftmargin + ($cols * $cardwidth);
            // DrawBoxes($cardX, $cardY); // debug
            DrawFront($cardX, $cardY, $title, $date, $ssid, $password);
        }
    }
    // Page 2
    $pdf->AddPage();
    for ($rows = 0; $rows < 5; $rows++) {
        $cardY = $topmargin + ($rows * $cardheight);
        for ($cols = 0; $cols < 2; $cols++) {
            $cardX = $leftmargin + ($cols * $cardwidth);
            // DrawBoxes($cardX, $cardY); // debug
            DrawBack($cardX, $cardY, $ssid, $password, $disclaimer, $type);
        }
    }
}
$title = $ssid = $password = $date = $disclaimer = $simple = '';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $title = trim(stripslashes(htmlspecialchars($_POST['title'])));
  $ssid = trim(stripslashes(htmlspecialchars($_POST['ssid'])));
  $password = trim(stripslashes(htmlspecialchars($_POST['password'])));
  $date = trim(stripslashes(htmlspecialchars($_POST['date'])));
  $disclaimer = trim(stripslashes(htmlspecialchars($_POST['disclaimer'])));
  $simple = trim(stripslashes(htmlspecialchars($_POST['simple'])));
} else {
  header('Location: wifiqrbusinesscards.php');
  die();
}
if ($title && $ssid && $password && $date && $disclaimer) {
  $validated = 1;
  unset($_POST['title']);
  unset($_POST['ssid']);
  unset($_POST['password']);
  unset($_POST['date']);
  unset($_POST['disclaimer']);
  if (isset($_POST['simple'])) {
    unset($_POST['simple']);
  }
} else {
  header('Location: wifiqrbusinesscards.php');
  die();
}
if ($_POST) { // unwanted parameters is tampering
  foreach ($_POST as &$value) {
    header('Location: wifiqrbusinesscards.php');
    die();
  }
}
if ($simple) {
  DrawSSID($title, $date, $ssid, $password, $disclaimer, $simple);
} else {
  DrawSSID($title, $date, $ssid, $password, $disclaimer);
}
$pdf->Output();
?>