<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Wifi Cards</title>
<style>
.title {
  background-color : #4fed78;
}
.updated {
  background-color : #69e2f8;
}
.ssid {
  background-color : #f98fee;
}
.password {
  background-color : #f6af69;
}
.disclaimer {
  background-color : #f1ff7a;
}
</style>
</head>
<body>
<img style="float: right; margin: 0px 15px 15px 0px;border-style: dotted;" src="samplecard.png" width="200" />
<form action="wifiqrbusinesscardspdf.php" method="post">
<table>
<tr><th> &nbsp;</th><th>Enter Values</th></tr>
<tr><td>Card Title:</td><td><input type="text" name="title" value="GUEST WIFI ACCESS"  maxlength="32" onClick="this.select();" class="title"></td></tr>
<tr><td>SSID:</td><td><input type="text" name="ssid" value="Guest" maxlength="32" onClick="this.select();" class="ssid"></td></tr>
<tr><td>Password:</td><td><input type="password" name="password" maxlength="32" class="password"></td></tr>
<tr><td>Date updated:</td><td><input type="input" name="date" value="<?php echo(date("F j, Y")); ?>"maxlength="32" onClick="this.select();" class="updated"></td></tr>
<tr><td>Disclaimer:</td>
<td><textarea name="disclaimer" rows="4" cols="80" class="disclaimer">For personal use only</textarea></td></tr>
<tr><td>Design:</td><td><input type="checkbox" name="simple" value="simple">Simple design (no QR code)</td></tr>
</table>
<input type="submit">
</form>
<p><i>Compatible with Avery business card stock (e.g., 5371). Print both sides then fold to snap out cards.</i></p>
</body>